package be.enverbral.codegenerator

import com.intellij.psi.PsiClass

data class GenerateMapperParameters(
    val sourceClass: PsiClass,
    val targetClass: PsiClass,
    val mapperClassName: String,
    val copyStrategy: CopyStrategy,
)