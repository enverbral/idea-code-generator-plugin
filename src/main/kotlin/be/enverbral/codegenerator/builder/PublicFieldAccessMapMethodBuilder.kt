package be.enverbral.codegenerator.builder

import be.enverbral.codegenerator.MapMethodInterface
import be.enverbral.codegenerator.utils.JavaNaming
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiField
import com.intellij.psi.PsiMethod

class PublicFieldAccessMapMethodBuilder(
    methodInterface: MapMethodInterface,
    private val elementFactory: PsiElementFactory,
) : MapMethodBuilder {
    private val sourceVariable = JavaNaming.toVariableName(methodInterface.sourceClass.name!!)
    private val targetVariable = JavaNaming.toVariableName(methodInterface.targetClass.name!!)
    private val method = createEmptyMapMethod(methodInterface, elementFactory)
    private val methodBody
        get() = method.body!!
    private val targetClass = methodInterface.targetClass

    override fun beginMethod() {
        methodBody.add(elementFactory.createStatementFromText("${targetClass.qualifiedName} $targetVariable = new ${targetClass.qualifiedName}();", null))
    }

    override fun copyNonMatchingField(sourceField: PsiField) {
        methodBody.add(elementFactory.createStatementFromText("; // TODO: $targetVariable.${sourceField.name} = $sourceVariable.${sourceField.name};", null))
    }

    override fun copyPrimitiveField(sourceFieldGetter: String, targetField: PsiField) {
        methodBody.add(elementFactory.createStatementFromText("$targetVariable.${targetField.name} = $sourceVariable.$sourceFieldGetter;", null))
    }

    override fun copyComplexField(sourceFieldGetter: String, targetField: PsiField, mapMethod: PsiMethod) {
        methodBody.add(
            elementFactory.createStatementFromText(
                "$targetVariable.${targetField.name} = ${mapMethod.name}($sourceVariable.$sourceFieldGetter);",
                null
            )
        )
    }

    override fun endMethod() {
        methodBody.add(elementFactory.createStatementFromText("return $targetVariable;", null))
    }

    override fun build() = method

    private fun createEmptyMapMethod(methodInterface: MapMethodInterface, elementFactory: PsiElementFactory) =
        elementFactory.createMethodFromText("${methodInterface.asString()} {}", null)
}