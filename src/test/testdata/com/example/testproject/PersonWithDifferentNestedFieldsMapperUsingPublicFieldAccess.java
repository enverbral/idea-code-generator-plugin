package com.example.testproject;

public class PersonWithDifferentNestedFieldsMapperUsingPublicFieldAccess {
    public PersonWithDifferentNestedFieldsDto toPersonWithDifferentNestedFieldsDto(PersonWithDifferentNestedFields personWithDifferentNestedFields) {
        PersonWithDifferentNestedFieldsDto personWithDifferentNestedFieldsDto = new PersonWithDifferentNestedFieldsDto();
        personWithDifferentNestedFieldsDto.name = toNameDto(personWithDifferentNestedFields.name());
        personWithDifferentNestedFieldsDto.age = toAgeDto(personWithDifferentNestedFields.age());
        return personWithDifferentNestedFieldsDto;
    }

    private PersonWithDifferentNestedFieldsDto.AgeDto toAgeDto(PersonWithDifferentNestedFields.Age age) {
        PersonWithDifferentNestedFieldsDto.AgeDto ageDto = new PersonWithDifferentNestedFieldsDto.AgeDto();
        ageDto.age = age.age;
        return ageDto;
    }

    private PersonWithDifferentNestedFieldsDto.NameDto toNameDto(PersonWithDifferentNestedFields.Name name) {
        PersonWithDifferentNestedFieldsDto.NameDto nameDto = new PersonWithDifferentNestedFieldsDto.NameDto();
        nameDto.name = name.name;
        return nameDto;
    }
}
