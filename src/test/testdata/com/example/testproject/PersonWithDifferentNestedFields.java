package com.example.testproject;

public class PersonWithDifferentNestedFields<caret> {

    private final Name name;
    private final Age age;

    public PersonWithDifferentNestedFields(Name name, Age age) {
        this.name = name;
        this.age = age;
    }

    public Name name() {
        return name;
    }

    public Age age() {
        return age;
    }

    public static class Name {
        private final String name;

        public Name(String name) {
            this.name = name;
        }

        public String get() {
            return name;
        }
    }

    public static class Age {
        private final int age;

        public Age(int age) {
            this.age = age;
        }

        public int get() {
            return age;
        }
    }

}
