package com.example.testproject;

public class PersonWithDifferentNestedFieldsDto<caret> {

    private final NameDto name;
    private final AgeDto age;

    public PersonWithDifferentNestedFieldsDto(NameDto name, AgeDto age) {
        this.name = name;
        this.age = age;
    }

    public NameDto name() {
        return name;
    }

    public AgeDto age() {
        return age;
    }

    public static class NameDto {
        private final String name;

        public NameDto(String name) {
            this.name = name;
        }

        public String get() {
            return name;
        }
    }

    public static class AgeDto {
        private final int age;

        public AgeDto(int age) {
            this.age = age;
        }

        public int get() {
            return age;
        }
    }

}
