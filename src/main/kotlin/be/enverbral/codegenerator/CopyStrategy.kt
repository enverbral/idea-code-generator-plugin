package be.enverbral.codegenerator

enum class CopyStrategy(val displayName: String) {
    PUBLIC_FIELDS("Public fields"),
    INNER_BUILDER("Inner builder"),
    ;

    override fun toString() = displayName
}