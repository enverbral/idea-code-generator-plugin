package be.enverbral.codegenerator.utils

enum class MethodVisibility {
    PUBLIC,
    PRIVATE,
    ;

    override fun toString() = name.lowercase()
}