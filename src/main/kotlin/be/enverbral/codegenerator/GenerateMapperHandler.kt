package be.enverbral.codegenerator

import be.enverbral.codegenerator.builder.InnerBuilderMapMethodBuilder
import be.enverbral.codegenerator.builder.PublicFieldAccessMapMethodBuilder
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VirtualFileManager
import com.intellij.psi.*

object GenerateMapperHandler {
    private val METHOD_BUILDER_FACTORY_BY_COPY_STRATEGY: Map<CopyStrategy, MapMethodBuilderFactory> = mapOf(
        CopyStrategy.PUBLIC_FIELDS to ::PublicFieldAccessMapMethodBuilder,
        CopyStrategy.INNER_BUILDER to ::InnerBuilderMapMethodBuilder,
    )

    fun generateMapper(
        project: Project,
        parameters: GenerateMapperParameters,
    ) {
        generateMapper(
            project,
            parameters.sourceClass,
            parameters.targetClass,
            parameters.mapperClassName,
            METHOD_BUILDER_FACTORY_BY_COPY_STRATEGY.getValue(parameters.copyStrategy),
        )
    }

    private fun generateMapper(
        project: Project,
        sourceClass: PsiClass,
        targetClass: PsiClass,
        mapperClassName: String,
        methodBuilderFactory: MapMethodBuilderFactory,
    ) {
        val currentDirectory: PsiDirectory = sourceClass.containingFile.containingDirectory
        val elementFactory: PsiElementFactory = JavaPsiFacade.getInstance(project).elementFactory

        WriteCommandAction.runWriteCommandAction(project) {
            val createdClass = createMapperClass(
                elementFactory = elementFactory,
                methodBuilderFactory = methodBuilderFactory,
                mapperName = mapperClassName,
                sourceClass = sourceClass,
                targetClass = targetClass,
                currentDirectory = currentDirectory,
            )
            FileEditorManager.getInstance(project).openFile(createdClass.containingFile.virtualFile, true)
        }

        VirtualFileManager.getInstance().refreshWithoutFileWatcher(true)
    }

    private fun createMapperClass(
        elementFactory: PsiElementFactory,
        methodBuilderFactory: MapMethodBuilderFactory,
        mapperName: String,
        sourceClass: PsiClass,
        targetClass: PsiClass,
        currentDirectory: PsiDirectory,
    ): PsiElement {
        val mapperClass = elementFactory.createClass(mapperName)

        val mapMethods = MapMethodGenerator(methodBuilderFactory)
            .generateMapMethods(
                sourceClass = sourceClass,
                targetClass = targetClass,
                elementFactory = elementFactory,
            )
        mapMethods.reversed().forEach { mapperClass.add(it) }

        return currentDirectory.add(mapperClass)
    }

}