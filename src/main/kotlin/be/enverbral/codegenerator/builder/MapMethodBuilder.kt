package be.enverbral.codegenerator.builder

import com.intellij.psi.PsiField
import com.intellij.psi.PsiMethod

interface MapMethodBuilder {
    fun beginMethod()
    fun copyNonMatchingField(sourceField: PsiField)
    fun copyPrimitiveField(sourceFieldGetter: String, targetField: PsiField)
    fun copyComplexField(sourceFieldGetter: String, targetField: PsiField, mapMethod: PsiMethod)
    fun endMethod()
    fun build(): PsiMethod
}