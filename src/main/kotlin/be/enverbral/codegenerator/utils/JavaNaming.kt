package be.enverbral.codegenerator.utils

object JavaNaming {
    fun toVariableName(name: String) = name[0].lowercaseChar() + name.substring(1)

    fun toBeanGetter(name: String) = methodOf("get", name)

    fun toBooleanBeanGetter(name: String) = methodOf("is", name)

    fun toRecordGetter(name: String) = toVariableName(name)

    fun toBuilderSetter(name: String) = methodOf("with", name)

    private fun methodOf(prefix: String, property: String) = prefix + property[0].uppercase() + property.substring(1)

}