package com.example.testproject;

public class PersonWithEnums<caret> {

    private final String name;
    private final Status status;
    private final String sex;
    private final EmailStatus emailStatus;
    private final ShirtSize shirtSize;

    public PersonWithEnums(int id, Status status, String sex, EmailStatus emailStatus, ShirtSize shirtSize) {
        this.id = id;
        this.status = status;
        this.sex = sex;
        this.emailStatus = emailStatus;
        this.shirtSize = shirtSize;
    }

    public String getName() {
        return name;
    }

    public Status getStatus() {
        return status;
    }

    public String getSex() {
        return sex;
    }

    public EmailStatus getEmailStatus() {
        return emailStatus;
    }

    public ShirtSize getShirtSize() {
        return shirtSize;
    }

    public static enum Status {
        ENABLED, DISABLED;
    }

    public static enum EmailStatus {
        EMAIL_SENT, EMAIL_CONFIRMED;
    }

    public static ShirtSize {
        SMALL, MEDIUM, LARGE;
    }
}
