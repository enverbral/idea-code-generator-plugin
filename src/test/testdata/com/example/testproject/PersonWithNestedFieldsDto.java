package com.example.testproject;

public class PersonWithNestedFieldsDto {

    private final String name;
    private final int age;
    private final AddressDto address;

    public PersonWithNestedFieldsDto(String name, int age, AddressDto address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public String name() {
        return name;
    }

    public int age() {
        return age;
    }

    public AddressDto address() {
        return address;
    }

    public static class AddressDto {
        private final String street;
        private final String city;
        private final CoordinatesDto coordinates;
        private final int zipcode;

        public AddressDto(String street, String city, CoordinatesDto coordinates, int zipcode) {
            this.street = street;
            this.city = city;
            this.coordinates = coordinates;
            this.zipcode = zipcode;
        }

        public String street() {
            return street;
        }

        public String city() {
            return city;
        }

        public CoordinatesDto coordinates() {
            return coordinates;
        }

        public int zipcode() {
            return zipcode;
        }
    }

    public static class CoordinatesDto {
        private final double latitude;
        private final double longitude;

        public CoordinatesDto(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public double latitude() {
            return latitude;
        }

        public double longitude() {
            return longitude;
        }
    }
}
