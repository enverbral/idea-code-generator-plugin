package be.enverbral.codegenerator

data class GenerateMapperDialogData(
    val sourceClassName: String,
    val targetClassName: String,
    val mapperClassName: String,
    val copyStrategy: CopyStrategy,
)
