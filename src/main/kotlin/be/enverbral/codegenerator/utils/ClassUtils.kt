package be.enverbral.codegenerator.utils

import com.intellij.openapi.project.Project
import com.intellij.psi.JavaPsiFacade
import com.intellij.psi.PsiClass
import com.intellij.psi.search.GlobalSearchScope

object ClassUtils {
    fun findClassByName(project: Project, className: String): PsiClass? {
        val scope = GlobalSearchScope.projectScope(project)
        return JavaPsiFacade.getInstance(project).findClass(className, scope)
    }
}