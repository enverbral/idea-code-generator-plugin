package com.example.testproject;

public class CustomerWithTwoAddressesMapperUsingPublicFieldAccess {
    public CustomerWithTwoAddressesDto toCustomerWithTwoAddressesDto(CustomerWithTwoAddresses customerWithTwoAddresses) {
        CustomerWithTwoAddressesDto customerWithTwoAddressesDto = new CustomerWithTwoAddressesDto();
        customerWithTwoAddressesDto.name = customerWithTwoAddresses.name();
        customerWithTwoAddressesDto.invoiceAddress = toAddressDto(customerWithTwoAddresses.invoiceAddress());
        customerWithTwoAddressesDto.billingAddress = toAddressDto(customerWithTwoAddresses.billingAddress());
        return customerWithTwoAddressesDto;
    }

    private CustomerWithTwoAddressesDto.AddressDto toAddressDto(CustomerWithTwoAddresses.Address address) {
        CustomerWithTwoAddressesDto.AddressDto addressDto = new CustomerWithTwoAddressesDto.AddressDto();
        addressDto.street = address.street();
        addressDto.city = address.city();
        addressDto.zipcode = address.zipcode();
        return addressDto;
    }
}
