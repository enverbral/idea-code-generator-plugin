package com.example.testproject;

public class PersonDto {

    private final int id;
    private final String name;
    private final int age;
    private final boolean active;
    private final String iDontExistInPerson;

    public PersonDto(int id, String name, int age, boolean active, String iDontExistInPerson) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.active = active;
        this.iDontExistInPerson = iDontExistInPerson;
    }

    public int id() {
        return id;
    }

    public String name() {
        return name;
    }

    public int age() {
        return age;
    }

    public boolean isActive() {
        return active;
    }

    public String iDontExistInPerson() {
        return iDontExistInPerson;
    }
}
