package be.enverbral.codegenerator

import be.enverbral.codegenerator.utils.ClassUtils.findClassByName
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.PlatformDataKeys
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiJavaFile
import com.intellij.psi.util.PsiTreeUtil

open class GenerateMapperAction : AnAction() {
    override fun update(e: AnActionEvent) {
        e.presentation.isVisible = javaClassOpenInEditor(e) != null
    }

    override fun actionPerformed(e: AnActionEvent) {
        val project: Project = e.project ?: return
        val parameters = gatherParameters(e) ?: return

        GenerateMapperHandler.generateMapper(project, parameters)
    }

    private fun gatherParameters(e: AnActionEvent): GenerateMapperParameters? {
        val project = e.project ?: return null
        val sourceClass = javaClassOpenInEditor(e) ?: return null
        val dialogData = showGenerateMapperDialog(project, sourceClass) ?: return null

        return GenerateMapperParameters(
            sourceClass = findClassByName(project, dialogData.sourceClassName) ?: return null,
            targetClass = findClassByName(project, dialogData.targetClassName) ?: return null,
            mapperClassName = dialogData.mapperClassName,
            copyStrategy = dialogData.copyStrategy,
        )
    }

    private fun javaClassOpenInEditor(e: AnActionEvent): PsiClass? {
        val psiFile = javaFileOpenInEditor(e) ?: return null
        return classInFile(psiFile)
    }

    private fun classInFile(psiFile: PsiJavaFile) = PsiTreeUtil.getChildOfType(psiFile, PsiClass::class.java)

    private fun javaFileOpenInEditor(e: AnActionEvent): PsiJavaFile? {
        val psiFile = fileOpenInEditor(e)
        if (psiFile !is PsiJavaFile) {
            return null
        }

        return psiFile
    }

    private fun fileOpenInEditor(e: AnActionEvent): PsiFile? {
        val project = e.project ?: return null
        val editor: Editor = e.getData(PlatformDataKeys.EDITOR) ?: return null
        return PsiDocumentManager.getInstance(project).getPsiFile(editor.document)
    }

    protected open fun showGenerateMapperDialog(project: Project, sourceClass: PsiClass) =
        GenerateMapperDialog.showGenerateMapperDialog("Select target class", project, sourceClass)

}