package com.example.plugindemo

import be.enverbral.codegenerator.CopyStrategy
import be.enverbral.codegenerator.CopyStrategy.INNER_BUILDER
import be.enverbral.codegenerator.CopyStrategy.PUBLIC_FIELDS
import be.enverbral.codegenerator.GenerateMapperAction
import be.enverbral.codegenerator.GenerateMapperDialogData
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiClass
import com.intellij.testFramework.fixtures.LightJavaCodeInsightFixtureTestCase

private const val TESTPROJECT_ROOT_PACKAGE = "com/example/testproject"

class GenerateMapperActionTest : LightJavaCodeInsightFixtureTestCase() {

    override fun getTestDataPath() = "src/test/testdata"

    fun test_generatesMapMethods_forSimpleClass_usingPublicAccessFields() {
        copyTestClasses("PersonDto", "Person")

        generateMapperClass(
            sourceClassName = "Person",
            targetClassName = "PersonDto",
            mapperClassName = "PersonMapperUsingPublicFieldAccess",
            copyStrategy = PUBLIC_FIELDS,
        )

        verifyMapperClassContent("PersonMapperUsingPublicFieldAccess")
    }

    fun test_generatesMapMethods_forSimpleClass_usingInnerBuilder() {
        copyTestClasses("PersonDtoWithInnerBuilder", "Person")

        generateMapperClass(
            sourceClassName = "Person",
            targetClassName = "PersonDtoWithInnerBuilder",
            mapperClassName = "PersonMapperUsingInnerBuilder",
            copyStrategy = INNER_BUILDER,
        )

        verifyMapperClassContent("PersonMapperUsingInnerBuilder")
    }

    fun test_generatesMapMethodsForNestedFields_givenClassWithNestedFields() {
        copyTestClasses("PersonWithNestedFields", "PersonWithNestedFieldsDto")

        generateMapperClass(
            sourceClassName = "PersonWithNestedFields",
            targetClassName = "PersonWithNestedFieldsDto",
            mapperClassName = "PersonWithNestedFieldsMapperUsingPublicFieldAccess",
            copyStrategy = PUBLIC_FIELDS,
        )

        verifyMapperClassContent("PersonWithNestedFieldsMapperUsingPublicFieldAccess")
    }

    fun test_generatesSingleMappingMethod_forMultipleNestedFieldsOfTheSameType() {
        copyTestClasses("CustomerWithTwoAddresses", "CustomerWithTwoAddressesDto")

        generateMapperClass(
            sourceClassName = "CustomerWithTwoAddresses",
            targetClassName = "CustomerWithTwoAddressesDto",
            mapperClassName = "CustomerWithTwoAddressesMapperUsingPublicFieldAccess",
            copyStrategy = PUBLIC_FIELDS,
        )

        verifyMapperClassContent("CustomerWithTwoAddressesMapperUsingPublicFieldAccess")
    }

    fun test_generatesMapMethodsForAllNestedFields_givenClassWithNestedFieldsOfDifferentTypes() {
        copyTestClasses("PersonWithDifferentNestedFields", "PersonWithDifferentNestedFieldsDto")

        generateMapperClass(
            sourceClassName = "PersonWithDifferentNestedFields",
            targetClassName = "PersonWithDifferentNestedFieldsDto",
            mapperClassName = "PersonWithDifferentNestedFieldsMapperUsingPublicFieldAccess",
            copyStrategy = PUBLIC_FIELDS,
        )

        verifyMapperClassContent("PersonWithDifferentNestedFieldsMapperUsingPublicFieldAccess")
    }

    fun test_generatesMapMethodsForEnumValues_givenSourceOrTargetIsStringOrEnum() {
        copyTestClasses("PersonWithEnums", "PersonWithEnumsDto")

        generateMapperClass(
            sourceClassName = "PersonWithEnums",
            targetClassName = "PersonWithEnumsDto",
            mapperClassName = "PersonWithEnumsMapperUsingPublicFieldAccess",
            copyStrategy = PUBLIC_FIELDS,
        )

        verifyMapperClassContent("PersonWithEnumsMapperUsingPublicFieldAccess")
    }

    private fun copyTestClasses(vararg classNames: String) {
        myFixture.configureByFiles(*classNames.map { filePathOfClass(it) }.toTypedArray())
    }

    private fun generateMapperClass(sourceClassName: String, targetClassName: String, mapperClassName: String, copyStrategy: CopyStrategy) {
        myFixture.configureByFile(filePathOfClass(sourceClassName))
        myFixture.testAction(
            TestableGenerateMapperAction(
                targetClassFqn = toFqn(targetClassName),
                mapperClassName = mapperClassName,
                copyStrategy = copyStrategy,
            )
        )
    }

    private fun verifyMapperClassContent(mapperClassName: String) {
        myFixture.checkResultByFile(filePathOfClass(mapperClassName), filePathOfClass(mapperClassName), false)
    }

    private fun filePathOfClass(className: String) = "$TESTPROJECT_ROOT_PACKAGE/$className.java"

    private fun toFqn(sourceClassName: String) = "com.example.testproject.$sourceClassName"

    class TestableGenerateMapperAction(
        private val targetClassFqn: String,
        private val mapperClassName: String,
        private val copyStrategy: CopyStrategy,
    ) : GenerateMapperAction() {
        override fun showGenerateMapperDialog(project: Project, sourceClass: PsiClass) = GenerateMapperDialogData(
            sourceClassName = sourceClass.qualifiedName!!,
            targetClassName = targetClassFqn,
            copyStrategy = copyStrategy,
            mapperClassName = mapperClassName,
        )
    }

}