package com.example.testproject;

public class PersonMapperUsingPublicFieldAccess {
    public PersonDto toPersonDto(Person person) {
        PersonDto personDto = new PersonDto();
        personDto.id = person.id;
        personDto.name = person.getName();
        personDto.age = person.age();
        personDto.active = person.isActive();
        ; // TODO: personDto.iDontExistInPersonDto = person.iDontExistInPersonDto;
        return personDto;
    }
}
