package com.example.testproject;

public class PersonWithNestedFields<caret> {

    private final String name;
    private final int age;
    private final Address address;

    public PersonWithNestedFields(String name, int age, Address address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public String name() {
        return name;
    }

    public int age() {
        return age;
    }

    public Address address() {
        return address;
    }

    public static class Address {
        private final String street;
        private final String city;
        private final Coordinates coordinates;
        private final int zipcode;

        public Address(String street, String city, Coordinates coordinates, int zipcode) {
            this.street = street;
            this.city = city;
            this.coordinates = coordinates;
            this.zipcode = zipcode;
        }

        public String street() {
            return street;
        }

        public String city() {
            return city;
        }

        public Coordinates coordinates() {
            return coordinates;
        }

        public int zipcode() {
            return zipcode;
        }
    }

    public static class Coordinates {
        private final double latitude;
        private final double longitude;

        public Coordinates(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public double latitude() {
            return latitude;
        }

        public double longitude() {
            return longitude;
        }
    }
}
