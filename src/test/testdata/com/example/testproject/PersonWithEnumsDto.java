package com.example.testproject;

public class PersonWithEnumsDto {

    private final String name;
    private final String status;
    private final Sex sex;
    private final int emailStatus;
    private final ShirtSize shirtSize;

    public PersonWithEnumsDto(String name, String status, Sex sex, int emailStatus, ShirtSize shirtSize) {
        this.name = name;
        this.status = status;
        this.sex = sex;
        this.emailStatus = emailStatus;
        this.shirtSize = shirtSize;
    }

    public int id() {
        return id;
    }

    public String status() {
        return status;
    }

    public Sex sex() {
        return sex;
    }

    public int emailStatus() {
        return emailStatus;
    }

    public ShirtSize shirtSize() {
        return shirtSize;
    }

    public static enum Sex {
        MALE, FEMALE, OTHER;
    }

    public static enum ShirtSize {
        SMALL, MEDIUM, LARGE;
    }
}
