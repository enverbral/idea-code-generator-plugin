package be.enverbral.codegenerator

import be.enverbral.codegenerator.utils.ClassUtils.findClassByName
import com.intellij.ide.util.TreeClassChooserFactory
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.ComboBox
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.psi.JavaCodeFragment
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiModifier
import com.intellij.psi.search.GlobalSearchScope
import com.intellij.refactoring.RefactoringBundle
import com.intellij.ui.ReferenceEditorComboWithBrowseButton
import java.awt.GridLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.JComponent
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JTextField

// inspired by the MoveMembersDialog of the intellij refactoring plugin
internal class GenerateMapperDialog(
    title: String,
    private val project: Project,
    private val originalSourceClass: PsiClass
) : DialogWrapper(project) {
    private val sourceClassNameEditorCombo: ReferenceEditorComboWithBrowseButton
    private val targetClassNameEditorCombo: ReferenceEditorComboWithBrowseButton
    private val copyStrategyComboBox: ComboBox<CopyStrategy>
    private val mapperClassNameTextField: JTextField

    init {
        setTitle(title)
        sourceClassNameEditorCombo = ReferenceEditorComboWithBrowseButton(
            ChooseClassAction(::sourceClassName),
            originalSourceClass.qualifiedName,
            project,
            true,
            JavaCodeFragment.VisibilityChecker.PROJECT_SCOPE_VISIBLE,
            RECENTS_KEY
        )
        targetClassNameEditorCombo = ReferenceEditorComboWithBrowseButton(
            ChooseClassAction(::targetClassName),
            "",
            project,
            true,
            JavaCodeFragment.VisibilityChecker.PROJECT_SCOPE_VISIBLE,
            RECENTS_KEY
        )
        copyStrategyComboBox = ComboBox(CopyStrategy.values())
        mapperClassNameTextField = JTextField()
        init()
    }

    val sourceClassName: String
        get() = sourceClassNameEditorCombo.text

    val targetClassName: String
        get() = targetClassNameEditorCombo.text

    val mapperClassName: String
        get() = mapperClassNameTextField.text

    val copyStrategy: CopyStrategy
        get() = copyStrategyComboBox.item

    val data: GenerateMapperDialogData
        get() = GenerateMapperDialogData(
            sourceClassName = sourceClassName,
            targetClassName = targetClassName,
            mapperClassName = mapperClassName,
            copyStrategy = copyStrategy
        )

    override fun createCenterPanel(): JComponent {
        val panel = JPanel(GridLayout(4, 2))
        panel.add(JLabel("Source class:"))
        panel.add(sourceClassNameEditorCombo)
        panel.add(JLabel("Target class:"))
        panel.add(targetClassNameEditorCombo)
        panel.add(JLabel("Mapper class name:"))
        panel.add(mapperClassNameTextField)
        panel.add(JLabel("Copy strategy:"))
        panel.add(copyStrategyComboBox)
        return panel
    }

    private inner class ChooseClassAction(private val currentClassName: () -> String) : ActionListener {
        override fun actionPerformed(e: ActionEvent) {
            val chooser = TreeClassChooserFactory.getInstance(project).createWithInnerClassesScopeChooser(
                RefactoringBundle.message("choose.destination.class"), GlobalSearchScope.projectScope(project),
                { aClass: PsiClass -> aClass.parent is PsiFile || aClass.hasModifierProperty(PsiModifier.STATIC) }, null
            )
            val currentClass = findClassByName(project, currentClassName()) ?: originalSourceClass
            chooser.selectDirectory(currentClass.containingFile.containingDirectory)
            chooser.showDialog()
            val selectedClass = chooser.selected
            if (selectedClass != null) {
                targetClassNameEditorCombo.text = selectedClass.qualifiedName
            }
        }
    }

    companion object {
        private const val RECENTS_KEY = "GenerateMapperDialog.RECENTS_KEY"

        fun showGenerateMapperDialog(title: String, project: Project, sourceClass: PsiClass): GenerateMapperDialogData? {
            val dialog = GenerateMapperDialog(title, project, sourceClass)
            dialog.show()

            return if (dialog.exitCode == OK_EXIT_CODE) {
                dialog.data
            } else {
                null
            }
        }
    }
}
