package be.enverbral.codegenerator

import be.enverbral.codegenerator.builder.MapMethodBuilder
import be.enverbral.codegenerator.utils.ClassUtils
import be.enverbral.codegenerator.utils.JavaNaming
import be.enverbral.codegenerator.utils.MethodVisibility
import com.intellij.lang.jvm.JvmModifier
import com.intellij.psi.PsiClass
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiField
import com.intellij.psi.PsiMethod

private val NO_GENERATED_METHODS: List<MapMethod> = listOf()

typealias MapMethodBuilderFactory = (MapMethodInterface, PsiElementFactory) -> MapMethodBuilder

private data class MapMethod(
    val methodInterface: MapMethodInterface,
    val method: PsiMethod,
) {
    fun mapsBetween(sourceClass: PsiClass, targetClass: PsiClass) = methodInterface.mapsBetween(sourceClass, targetClass)
}

class MapMethodGenerator(private val methodBuilderFactory: MapMethodBuilderFactory) {
    fun generateMapMethods(
        sourceClass: PsiClass,
        targetClass: PsiClass,
        elementFactory: PsiElementFactory,
    ): List<PsiMethod> {
        val methodInterface = MapMethodInterface(
            sourceClass = sourceClass,
            targetClass = targetClass,
            visibility = MethodVisibility.PUBLIC,
        )

        return generateMapMethodRecursively(
            methodInterface = methodInterface,
            elementFactory = elementFactory,
            previousGeneratedMethods = NO_GENERATED_METHODS,
        ).map { it.method }
    }

    private fun generateMapMethodRecursively(
        methodInterface: MapMethodInterface,
        elementFactory: PsiElementFactory,
        previousGeneratedMethods: List<MapMethod>,
    ): List<MapMethod> {
        val methodBuilder = methodBuilderFactory(methodInterface, elementFactory)

        val generatedMethods: MutableList<MapMethod> = mutableListOf()

        methodBuilder.beginMethod()

        for (sourceField in methodInterface.sourceClass.fields) {
            if (isStaticField(sourceField)) {
                continue
            }

            val targetField = methodInterface.targetClass.findFieldByName(sourceField.name, false)

            if (targetField == null) {
                methodBuilder.copyNonMatchingField(sourceField)
                continue
            }

            generatedMethods += copyField(
                sourceField,
                targetField,
                methodBuilder,
                elementFactory,
                previousGeneratedMethods + generatedMethods
            )
        }

        methodBuilder.endMethod()

        return generatedMethods + MapMethod(methodInterface, methodBuilder.build())
    }

    private fun copyField(
        sourceField: PsiField,
        targetField: PsiField,
        methodBuilder: MapMethodBuilder,
        elementFactory: PsiElementFactory,
        previousGeneratedMethods: List<MapMethod>
    ): List<MapMethod> {
        if (isPrimitiveField(sourceField) && isPrimitiveField(targetField)) {
            methodBuilder.copyPrimitiveField(decideFieldGetter(sourceField), targetField)
            return NO_GENERATED_METHODS
        } else {
            val sourceFieldClass = ClassUtils.findClassByName(sourceField.project, sourceField.type.canonicalText)
            val targetFieldClass = ClassUtils.findClassByName(targetField.project, targetField.type.canonicalText)

            if (sourceFieldClass == null || targetFieldClass == null) {
                methodBuilder.copyNonMatchingField(sourceField)
                return NO_GENERATED_METHODS
            }

            if (sourceFieldClass.isEnum || targetFieldClass.isEnum) {
                methodBuilder.copyNonMatchingField(sourceField)
                return NO_GENERATED_METHODS
            }

            val (fieldMapMethod, generatedMethods) = findOrGenerateMapMethodFor(
                sourceFieldClass,
                targetFieldClass,
                elementFactory,
                previousGeneratedMethods
            )
            methodBuilder.copyComplexField(decideFieldGetter(sourceField), targetField, fieldMapMethod.method)

            return generatedMethods
        }
    }

    private fun decideFieldGetter(field: PsiField): String {
        val containingClass = field.containingClass!!

        if (isBooleanField(field)) {
            val booleanBeanGetter = containingClass.findGetterForField(field, JavaNaming::toBooleanBeanGetter)
            if (booleanBeanGetter != null) return "${booleanBeanGetter.name}()"
        }

        val beanGetter = containingClass.findGetterForField(field, JavaNaming::toBeanGetter)
        if (beanGetter != null) return "${beanGetter.name}()"

        val recordGetter = containingClass.findGetterForField(field, JavaNaming::toRecordGetter)
        if (recordGetter != null) return "${recordGetter.name}()"

        return field.name
    }

    private fun findOrGenerateMapMethodFor(
        sourceFieldClass: PsiClass,
        targetFieldClass: PsiClass,
        elementFactory: PsiElementFactory,
        previousGeneratedMethods: List<MapMethod>,
    ): Pair<MapMethod, List<MapMethod>> {
        val fieldMapMethod = previousGeneratedMethods.find { it.mapsBetween(sourceFieldClass, targetFieldClass) }
        if (fieldMapMethod != null) {
            return fieldMapMethod to NO_GENERATED_METHODS
        }

        val generatedMethods = generateMapMethodRecursively(
            methodInterface = MapMethodInterface(
                visibility = MethodVisibility.PRIVATE,
                sourceClass = sourceFieldClass,
                targetClass = targetFieldClass,
            ),
            elementFactory = elementFactory,
            previousGeneratedMethods = previousGeneratedMethods,
        )

        return generatedMethods.last() to generatedMethods
    }

    private fun isPrimitiveField(sourceField: PsiField) = sourceField.type.canonicalText.startsWith("java") || !sourceField.type.canonicalText.contains('.')

    private fun isBooleanField(field: PsiField) = field.type.canonicalText == "boolean" || field.type.canonicalText == "java.lang.Boolean"

    private fun isStaticField(field: PsiField) = field.hasModifier(JvmModifier.STATIC)

}

internal fun PsiClass.findGetterForField(field: PsiField, namingConvention: (String) -> String) =
    methods.find { it.isGetterForField(field, namingConvention) }

internal fun PsiMethod.isGetterForField(field: PsiField, namingConvention: (String) -> String) =
    returnType == field.type && name == namingConvention(field.name)
