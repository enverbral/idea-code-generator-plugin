package com.example.testproject;

public class PersonDtoWithInnerBuilder {

    private final int id;
    private final String name;
    private final int age;
    private final boolean active;
    private final String iDontExistInPerson;

    private PersonDtoWithInnerBuilder(Builder builder) {
        id = builder.id;
        name = builder.name;
        age = builder.age;
        active = builder.active;
        iDontExistInPerson = builder.iDontExistInPerson;
    }

    public int id() { return id; }

    public String name() {
        return name;
    }

    public int age() {
        return age;
    }

    public boolean isActive() {
        return active;
    }

    public String iDontExistInPerson() {
        return iDontExistInPerson;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static final class Builder {
        private int id;
        private String name;
        private int age;
        private boolean active;
        private String iDontExistInPerson;

        private Builder() {
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withAge(int age) {
            this.age = age;
            return this;
        }

        public Builder withActive(boolean active) {
            this.active = active;
            return this;
        }

        public Builder withIDontExistInPerson(String iDontExistInPerson) {
            this.iDontExistInPerson = iDontExistInPerson;
            return this;
        }

        public PersonDtoWithInnerBuilder build() {
            return new PersonDtoWithInnerBuilder(this);
        }
    }


}
