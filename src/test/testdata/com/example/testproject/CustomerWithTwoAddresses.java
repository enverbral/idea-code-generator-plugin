package com.example.testproject;

public class CustomerWithTwoAddresses<caret> {

    private final String name;
    private final Address invoiceAddress;
    private final Address billingAddress;

    public CustomerWithTwoAddresses(String name, Address invoiceAddress, Address billingAddress) {
        this.name = name;
        this.invoiceAddress = invoiceAddress;
        this.billingAddress = billingAddress;
    }

    public String name() {
        return name;
    }

    public Address invoiceAddress() {
        return invoiceAddress;
    }

    public Address billingAddress() {
        return billingAddress;
    }

    public static class Address {
        private final String street;
        private final String city;
        private final int zipcode;

        public Address(String street, String city, int zipcode) {
            this.street = street;
            this.city = city;
            this.zipcode = zipcode;
        }

        public String street() {
            return street;
        }

        public String city() {
            return city;
        }

        public int zipcode() {
            return zipcode;
        }
    }
}
