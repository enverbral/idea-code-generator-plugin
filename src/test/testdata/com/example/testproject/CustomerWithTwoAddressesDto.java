package com.example.testproject;

public class CustomerWithTwoAddressesDto<caret> {

    private final String name;
    private final AddressDto invoiceAddress;
    private final AddressDto billingAddress;

    public CustomerWithTwoAddressesDto(String name, AddressDto invoiceAddress, AddressDto billingAddress) {
        this.name = name;
        this.invoiceAddress = invoiceAddress;
        this.billingAddress = billingAddress;
    }

    public String name() {
        return name;
    }

    public AddressDto invoiceAddress() {
        return invoiceAddress;
    }

    public AddressDto billingAddress() {
        return billingAddress;
    }

    public static class AddressDto {
        private final String street;
        private final String city;
        private final int zipcode;

        public AddressDto(String street, String city, int zipcode) {
            this.street = street;
            this.city = city;
            this.zipcode = zipcode;
        }

        public String street() {
            return street;
        }

        public String city() {
            return city;
        }

        public int zipcode() {
            return zipcode;
        }
    }
}
