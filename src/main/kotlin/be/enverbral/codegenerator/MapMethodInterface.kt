package be.enverbral.codegenerator

import be.enverbral.codegenerator.utils.JavaNaming
import be.enverbral.codegenerator.utils.MethodVisibility
import com.intellij.psi.PsiClass

data class MapMethodInterface(
    val visibility: MethodVisibility,
    val sourceClass: PsiClass,
    val targetClass: PsiClass,
) {
    fun asString() = toMethodInterface().asString()

    fun mapsBetween(sourceClass: PsiClass, targetClass: PsiClass) =
        sourceClass == this.sourceClass && targetClass == this.targetClass

    private fun toMethodInterface(): MethodInterface {
        val sourceVariable = JavaNaming.toVariableName(sourceClass.name!!)

        return MethodInterface(
            visibility = visibility,
            returnType = targetClass.qualifiedName!!,
            methodName = "to${targetClass.name}",
            parameters = listOf(
                "${sourceClass.qualifiedName} $sourceVariable"
            )
        )
    }
}

private data class MethodInterface(val visibility: MethodVisibility, val returnType: String, val methodName: String, val parameters: List<String>) {
    fun asString() = "$visibility $returnType ${methodName}(${parametersAsString()})"
    private fun parametersAsString() = parameters.joinToString(", ")
}
