package com.example.testproject;

public class PersonWithNestedFieldsMapperUsingPublicFieldAccess {
    public PersonWithNestedFieldsDto toPersonWithNestedFieldsDto(PersonWithNestedFields personWithNestedFields) {
        PersonWithNestedFieldsDto personWithNestedFieldsDto = new PersonWithNestedFieldsDto();
        personWithNestedFieldsDto.name = personWithNestedFields.name();
        personWithNestedFieldsDto.age = personWithNestedFields.age();
        personWithNestedFieldsDto.address = toAddressDto(personWithNestedFields.address());
        return personWithNestedFieldsDto;
    }

    private PersonWithNestedFieldsDto.AddressDto toAddressDto(PersonWithNestedFields.Address address) {
        PersonWithNestedFieldsDto.AddressDto addressDto = new PersonWithNestedFieldsDto.AddressDto();
        addressDto.street = address.street();
        addressDto.city = address.city();
        addressDto.coordinates = toCoordinatesDto(address.coordinates());
        addressDto.zipcode = address.zipcode();
        return addressDto;
    }

    private PersonWithNestedFieldsDto.CoordinatesDto toCoordinatesDto(PersonWithNestedFields.Coordinates coordinates) {
        PersonWithNestedFieldsDto.CoordinatesDto coordinatesDto = new PersonWithNestedFieldsDto.CoordinatesDto();
        coordinatesDto.latitude = coordinates.latitude();
        coordinatesDto.longitude = coordinates.longitude();
        return coordinatesDto;
    }
}
