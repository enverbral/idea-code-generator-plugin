package com.example.testproject;

public class PersonMapperUsingInnerBuilder {
    public PersonDtoWithInnerBuilder toPersonDtoWithInnerBuilder(Person person) {
        return PersonDtoWithInnerBuilder.newBuilder()
                .withId(person.id)
                .withName(person.getName())
                .withAge(person.age())
                .withActive(person.isActive())
                // TODO: .withIDontExistInPersonDto(person.iDontExistInPersonDto)
                .build();
    }
}
