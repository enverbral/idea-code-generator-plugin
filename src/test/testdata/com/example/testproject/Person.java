package com.example.testproject;

public class Person<caret> {

    public static final boolean IGNORE_STATIC_FIELD = true;

    public final int id;
    private final String name;
    private final int age;
    private boolean active;
    private final String iDontExistInPersonDto;

    public Person(int id, String name, int age, boolean active, String iDontExistInPersonDto) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.active = active;
        this.iDontExistInPersonDto = iDontExistInPersonDto;
    }

    public String getName() {
        return name;
    }

    public int age() {
        return age;
    }

    public boolean isActive() {
        return active;
    }

    public String iDontExistInPersonDto() {
        return iDontExistInPersonDto;
    }
}
