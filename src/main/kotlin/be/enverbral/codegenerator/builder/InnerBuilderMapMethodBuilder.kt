package be.enverbral.codegenerator.builder

import be.enverbral.codegenerator.MapMethodInterface
import be.enverbral.codegenerator.utils.JavaNaming
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiField
import com.intellij.psi.PsiMethod

class InnerBuilderMapMethodBuilder(
    methodInterface: MapMethodInterface,
    private val elementFactory: PsiElementFactory,
) : MapMethodBuilder {
    private val sourceVariable = JavaNaming.toVariableName(methodInterface.sourceClass.name!!)
    private val method = createEmptyMapMethod(methodInterface, elementFactory)
    private val methodBody
        get() = method.body!!
    private val targetClass = methodInterface.targetClass
    private var returnStatement = "";

    override fun beginMethod() {
        returnStatement += "return ${targetClass.qualifiedName}.newBuilder()\n"
    }

    override fun copyNonMatchingField(sourceField: PsiField) {
        returnStatement += "\t\t// TODO: ${with(sourceField) { "$sourceVariable.${sourceField.name}" }}"
    }

    override fun copyPrimitiveField(sourceFieldGetter: String, targetField: PsiField) {
        returnStatement += with(targetField) { "$sourceVariable.$sourceFieldGetter" }
    }

    override fun copyComplexField(sourceFieldGetter: String, targetField: PsiField, mapMethod: PsiMethod) {
        returnStatement += with(targetField) { "${mapMethod.name}($sourceVariable.$sourceFieldGetter)" }
    }

    override fun endMethod() {
        methodBody.add(elementFactory.createStatementFromText("$returnStatement.build();", null))
    }

    override fun build() = method

    private fun with(targetField: PsiField, sourceFieldProvider: () -> String) =
        ".${JavaNaming.toBuilderSetter(targetField.name)}(${sourceFieldProvider()})\n"

    private fun createEmptyMapMethod(methodInterface: MapMethodInterface, elementFactory: PsiElementFactory) =
        elementFactory.createMethodFromText("${methodInterface.asString()} {}", null)
}